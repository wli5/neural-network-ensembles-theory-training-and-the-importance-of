# README #

The python script is used to generate experimental results for paper "Neural Network Ensembles: Theory, Training, 
and the Importance of Explicit Diversity.", authored by Wenjing Li, Randy C. Paffenroth, and David Berthiaume. 
One may reproduce our results in the paper by rerunning our script.