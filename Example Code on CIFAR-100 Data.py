import torchvision
import torchvision.transforms as transforms
from torchvision.datasets import CIFAR100
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
import numpy as np
import torch
import matplotlib.pyplot as plt
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import random
import scipy
from scipy import stats
import torchvision.datasets as datasets
import torchvision.models as models
import copy
import pickle
import os
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
# pip install efficientnet_pytorch
from efficientnet_pytorch import EfficientNet


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

lambda_=torch.tensor([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]).to(device)

epochs =20
learning_rate = 0.001
ensemble_size=3
n_classes=100


# Transformations
input_size=224
transform_with_aug =transforms.Compose([
        transforms.RandomResizedCrop(input_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.247, 0.243, 0.261])
    ])


transform_no_aug   =transforms.Compose([
        transforms.Resize(input_size),
        transforms.CenterCrop(input_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.247, 0.243, 0.261])
    ])

# Downloading/Louding CIFAR100 data
trainset  = CIFAR100(root='./data', train=True , download=True, transform = transform_with_aug)
testset   = CIFAR100(root='./data', train=False, download=True, transform = transform_no_aug)


# Separating trainset/testset data/label
# x_train  = trainset.train_data
# x_test   = testset.test_data
# y_train  = trainset.train_labels
# y_test   = testset.test_labels

x_train = trainset.data
x_test = testset.data
y_train = trainset.targets
y_test = testset.targets


# Define a function to separate CIFAR classes by class index
def get_class_i(x, y, i):
    """
    x: trainset.train_data or testset.test_data
    y: trainset.train_labels or testset.test_labels
    i: class label, a number between 0 to 9
    return: x_i
    """
    # Convert to a numpy array
    y = np.array(y)
    # Locate position of labels that equal to i
    pos_i = np.argwhere(y == i)
    # Convert the result into a 1-D list
    pos_i = list(pos_i[:,0])
    # Collect all data that match the desired label
    x_i = [x[j] for j in pos_i]
    
    return x_i

class DatasetMaker(Dataset):
    def __init__(self, datasets, transformFunc = transform_no_aug):
        """
        datasets: a list of get_class_i outputs, i.e. a list of list of images for selected classes
        """
        self.datasets = datasets
        self.lengths  = [len(d) for d in self.datasets]
        self.transformFunc = transformFunc
    def __getitem__(self, i):
        class_label, index_wrt_class = self.index_of_which_bin(self.lengths, i)
        img = self.datasets[class_label][index_wrt_class]
        img = self.transformFunc(img)
        return img, class_label

    def __len__(self):
        return sum(self.lengths)
    
    def index_of_which_bin(self, bin_sizes, absolute_index, verbose=False):
        """
        Given the absolute index, returns which bin it falls in and which element of that bin it corresponds to.
        """
        # Which class/bin does i fall into?
        accum = np.add.accumulate(bin_sizes)
        if verbose:
            print("accum =", accum)
        bin_index  = len(np.argwhere(accum <= absolute_index))
        if verbose:
            print("class_label =", bin_index)
        # Which element of the fallent class/bin does i correspond to?
        index_wrt_class = absolute_index - np.insert(accum, 0, 0)[bin_index]
        if verbose:
            print("index_wrt_class =", index_wrt_class)

        return bin_index, index_wrt_class


kwargs = {'num_workers': 0, 'pin_memory': True}


cifarsubset_trainloader = torch.utils.data.DataLoader(trainset,shuffle=True, **kwargs, batch_size=32)
cifarsubset_testloader = torch.utils.data.DataLoader(testset,shuffle=False, **kwargs, batch_size=32)    
    


def cor(m, rowvar=False):
    '''Estimate a correlation matrix given data.
    Args:
        m: A 1-D or 2-D array containing multiple variables and observations.
            Each row of `m` represents a variable, and each column a single
            observation of all those variables.
        rowvar: If `rowvar` is True, then each row represents a
            variable, with observations in the columns. Otherwise, the
            relationship is transposed: each column represents a variable,
            while the rows contain observations.

    Returns:
        The correlation matrix of the variables.
    '''
    if m.dim() > 2:
        raise ValueError('m has more than 2 dimensions')
    if m.dim() < 2:
        m = m.view(1, -1)
    if not rowvar and m.size(0) != 1:
        m = m.t()
    m = m.type(torch.float)  # uncomment this line if desired
    fact = 1.0 / (m.size(1) - 1)
    m=m-torch.mean(m, dim=1, keepdim=True)
    #m -= torch.mean(m, dim=1, keepdim=True)
    mt = m.t()  # if complex: mt = m.t().conj()
    c=fact * m.matmul(mt).squeeze() #covariance matrix
    #normalize covariance matrix
    d_ = torch.diag(c)
    stddev = torch.pow(d_, 0.5)+1e-7  #Add 1e-7 to make the experiments numerically stable
    c = c.div(stddev.expand_as(c))
    c = c.div(stddev.expand_as(c).t())
    return c #correlation matrix




def d_func(matrix):
    Correlation=cor(matrix)
    L=Correlation.shape[1]-1
    d=(torch.sum(Correlation[1:L+1,1:L+1])-L)/2/(L*(L-1)/2)
    return d
def a_func(matrix):
    Correlation=cor(matrix)
    L=Correlation.shape[1]-1
    a=(torch.sum(Correlation[0,:])-1)/L
    return a

#Model 1: single model with cross entropy loss
#Model_1_1: efficientnet-b0
def efficientnet():
    net=EfficientNet.from_pretrained('efficientnet-b0')
    net._fc=nn.Linear(in_features=1280, out_features=n_classes, bias=True)
    return  net

net=efficientnet().to(device)

torch.backends.cudnn.benchmark=True
criterion=nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
train_loss=[]
best_acc = 0  # best test accuracy
for epoch in range(epochs):
#Training:
    net.train()
    for batch_idx, (inputs, targets) in enumerate(cifarsubset_trainloader):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()
        torch.cuda.empty_cache()
        train_loss.append(loss)

#Testing:
    net.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(cifarsubset_testloader):
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = net(inputs)
            loss = criterion(outputs, targets)
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()
            torch.cuda.empty_cache()
            
    
    acc = 100.*correct/total
        # Save checkpoint.
    if acc > best_acc:
        if not os.path.isdir('checkpoint'):
            os.mkdir('checkpoint')
        torch.save(net, './checkpoint/efficientnet_b0.pth')
        best_acc = acc

single_test_accuracy_efficientnet=best_acc 


plt.title('training loss')
plt.plot(train_loss)
plt.savefig('loss1.png')


data1 = ['single_test_accuracy_efficientnet',
         single_test_accuracy_efficientnet, 
         device,epochs]
with open('result1.pkl','wb') as outfile:
    pickle.dump(data1, outfile)

# with open('result1.pkl','rb') as infile:
#     result1 = pickle.load(infile)

    
    
# Model 2: truth-learner correlation + lambda_*learner-learner correlation

saved_efficientnet = torch.load('./checkpoint/efficientnet_b0.pth')
  
torch.backends.cudnn.benchmark=True
majority_vote_accu_test=[[] for i in range(len(lambda_))]
loss_train=[[] for i in range(len(lambda_))]
train_ave_truth_learner_corr=[[] for i in range(len(lambda_))]
train_ave_learner_learner_corr=[[] for i in range(len(lambda_))]
for i in range(len(lambda_)):
    model_list=[copy.deepcopy(saved_efficientnet),copy.deepcopy(saved_efficientnet),copy.deepcopy(saved_efficientnet)]
    params=[]
    for model in model_list:
        params.extend(list(model.parameters()))
    optimizer = torch.optim.Adam(params, lr=learning_rate) 
    best_acc=0
    for epoch in range(epochs):
        for j in range(ensemble_size):
            model_list[j].train()
        for batch_iter, data in enumerate(cifarsubset_trainloader):
        # get the inputs
            inputs_train, y_train = data
            labels_train=torch.FloatTensor((y_train[:,None] == torch.arange(0,n_classes)).numpy().astype(int))
            inputs_train,labels_train=inputs_train.to(device),labels_train.to(device)
            optimizer.zero_grad()
            m = nn.Softmax(dim=1)
            matrix_train=labels_train
            for j in range(ensemble_size):
                outputs_train=m(model_list[j](inputs_train))
                matrix_train=torch.cat((matrix_train,outputs_train),1)

            corr=cor(matrix_train) #the big correlation matrix for all truth and learners
            corr_new=corr[:n_classes*ensemble_size,n_classes:]
            
            a_train_new=0
            total_new=0
            for j in range(ensemble_size):
                a_train_new+=torch.sum(torch.diag(corr_new,j*n_classes)[:n_classes])
                total_new+=torch.sum(torch.diag(corr_new,j*n_classes))
            d_train_new=total_new-a_train_new
            loss=-a_train_new+lambda_[i]*d_train_new  #training loss
            loss_train[i].append(loss.item())
            loss.backward()
            optimizer.step()
            torch.cuda.empty_cache()
        #testing:
        total=0
        correct=0
        for j in range(ensemble_size):
            model_list[j].eval()
        with torch.no_grad():
            for batch_iter, test_data in enumerate(cifarsubset_testloader):
                inputs_test, y_test = test_data
                inputs_test, y_test = inputs_test.to(device), y_test.to(device)
                matrix_hard_test=torch.zeros([len(y_test),ensemble_size], dtype=torch.float)
                for j in range(ensemble_size):
                    output_test=model_list[j](inputs_test)
                    matrix_hard_test[:,j]=torch.max(output_test,1)[1]
                majority_vote_label_test=scipy.stats.mode(matrix_hard_test.cpu().numpy().T)[0]
                total += y_test.size(0)
                correct += (majority_vote_label_test == y_test.cpu().numpy()).sum().item()
                torch.cuda.empty_cache()
        acc = 100.*correct/total
        if acc > best_acc:
            best_acc = acc

    majority_vote_accu_test[i].append(best_acc)  


#save
data2 = ['majority_vote_accu_test, device,epochs',
         majority_vote_accu_test, device,epochs]

with open('result2.pkl','wb') as outfile:
    pickle.dump(data2, outfile)

# with open('result2.pkl','rb') as infile:
#     result2 = pickle.load(infile)  


 

